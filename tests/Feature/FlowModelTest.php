<?php

namespace CommunitasIT\Flowable\Tests\Feature;

use CommunitasIT\Flowable\Tests\FlowableTestCase;

use CommunitasIT\Flowable\Tests\Models\Ticket;

use \CommunitasIT\Flowable\Events\ModelTransitioned;

class FlowModelTest extends FlowableTestCase
{
    //use \Illuminate\Foundation\Testing\RefreshDatabase;

    /**
     * @test
     */
    function itCanCreateFlowableModels(){
        $ticket = $this->createTicket();

        $this->assertSame(1, Ticket::query()->count());
        $this->assertSame(1, $ticket->model_state()->count());
    }

    /**
     * @test
     */
    function itCanTransitionFlowableModelsViaTransition(){
        $ticket = $this->createTicket();
        $updated = $this->transitionTicket($ticket);

        $this->assertTrue($updated);
    }

    /**
     * @test
     */
    function itCanAuditTransitions(){
        $ticket = $this->createTicket();
        $this->transitionTicket($ticket);

        $this->assertSame(1, $ticket->workflow_audits()->count());
    }

    function createTicket(){
        return Ticket::create();
    }

    function transitionTicket($ticket){
        $transition = $ticket->getAvailableTransitions()->first();
        return $ticket->transition($transition);
    }
}