<?php

namespace CommunitasIT\Flowable\Tests\Models;

use Illuminate\Database\Eloquent\Model;

use CommunitasIT\Flowable\Flowable;

class Ticket extends Model
{
    use Flowable;

    protected $guarded = [];
    protected $table = 'tickets';
}