<?php

namespace CommunitasIT\Flowable\Tests\Unit;

use CommunitasIT\Flowable\Tests\FlowableTestCase;

use CommunitasIT\Flowable\Models\Workflow;
use CommunitasIT\Flowable\Models\WorkflowStatus;

class FlowStructureTest extends FlowableTestCase
{
    //use \Illuminate\Foundation\Testing\RefreshDatabase;

    /**
     * @group FlowStructure::Workflow
     * @test
     */
    function a_workflow_can_be_created(){
        $workflow = workflows()->new(__FUNCTION__);

        $this->assertSame(1, Workflow::where('name', __FUNCTION__)->count());
    }

    /**
     * @group FlowStructure::Workflow
     * @test
     */
    function a_workflow_cannot_have_the_same_name_as_another(){
        $workflow1 = workflows()->new(__FUNCTION__);

        $created_duplicate;
        try{
            $workflow2 = workflows()->new(__FUNCTION__);
            $created_duplicate = true;
        }catch(\Exception $e){
            $created_duplicate = false;
        }

        if($created_duplicate){
            $this->fail("A duplicate workflow was created");
        }else{
            $this->assertSame(1, 1);
        }
    }

    /**
     * @test
     */
    function a_status_can_be_created(){
        $status = workflows()->newStatus(
            __FUNCTION__,
            __FUNCTION__,
            config('flowable.types')[0]
        );

        $this->assertSame(1, WorkflowStatus::where('name', __FUNCTION__)->count());
    }

    /**
     * @test
     */
    function a_status_with_invalid_type_cannot_be_created(){
        $failed;
        try{
            $status = workflows()->newStatus(
                __FUNCTION__,
                __FUNCTION__,
                'some_string_that_should_not_be_in_config'
            );
            $failed = false;
        }catch(\Exception $e){
            $failed = true;
        }

        if($failed){
            $this->assertTrue(true);
        }else{
            $this->fail('A status with invalid type was created.');
        }
    }
}