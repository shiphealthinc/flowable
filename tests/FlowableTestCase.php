<?php

namespace CommunitasIT\Flowable\Tests;

use CommunitasIT\Flowable\FlowableServiceProvider;
use Orchestra\Testbench\TestCase;

class FlowableTestCase extends TestCase
{
    private $stubs = [
        "model_states" => "CreateModelStatesTable",
        "workflow_audits" => "CreateWorkflowAuditsTable",
        "workflow_has_models" => "CreateWorkflowHasModelsTable",
        "workflow_has_places" => "CreateWorkflowHasPlacesTable",
        "workflow_has_transitions" => "CreateWorkflowHasTransitionsTable",
        "workflow_statuses" => "CreateWorkflowStatusesTable",
        "workflows" => "CreateWorkflowsTable",
    ];

    public function setUp(): void
    {
        parent::setUp();
        
        /** load migrations for testing models */
        $this->loadMigrationsFrom(__DIR__ . '/database/migrations');

        $workflow = $this->initializeWorkflow();
        \CommunitasIT\Flowable\Tests\Models\Ticket::assignWorkflow($workflow);
    }

    protected function getPackageProviders($app)
    {
        return [
            FlowableServiceProvider::class,
        ];
    }

    protected function getEnvironmentSetUp($app)
    {
        /** Load stubbed migrations */
        $this->loadStubbedMigrations();
        $this->checkStubbedMigrations();

        // additional setup here
    }

    /**
     * Helper functions below
     */

    private function loadStubbedMigrations(){
        foreach($this->stubs as $stub => $class){
            $this->loadStubbedMigration(
                $stub,
                $class
            );
        }
    }

    private function resolveStubPath($stub){
        return __DIR__ . '/../database/migrations/' . $stub . '.stub';
    }

    private function checkStubbedMigrations(){
        $not_migrated = [];

        foreach(glob(__DIR__ . '/../database/migrations/*.stub') as $file){
            $basename = basename($file, '.stub');

            if(!in_array($basename, array_keys($this->stubs))){
                array_push($not_migrated, $basename);
            }
        }

        if($not_migrated){
            dd("The following migration stubs were not included in the test case setup: " . implode(", ", $not_migrated));
        }
    }

    private function loadStubbedMigration($stub, $class){
        include_once $this->resolveStubPath($stub);
        (new $class)->up();
    }

    private function initializeWorkflow(){
        $workflow = workflows()->new("Sample Test Workflow");

        $new = workflows()->newStatus("New", "Ticket is new", "new");
        $inprogress = workflows()->newStatus("In Progress", "Ticket is in progress", "inprogress");
        $done = workflows()->newStatus("Done", "Ticket is done", "done");

        $new_place = $workflow->addPlace($new->name, $new);
        $inprogress_place = $workflow->addPlace($inprogress->name, $inprogress);
        $done_place = $workflow->addPlace($done->name, $done);

        $new_place->addTransitionTo(
            $inprogress_place,
            $new_place->name . " to " . $inprogress_place->name
        );
        $inprogress_place->addTransitionTo(
            $done_place,
            $inprogress_place->name . " to " . $done_place->name
        );

        $workflow->setStartingPlace($new_place);

        return $workflow;
    }
}