# Flowable Documentation

## Setup
Run the following composer command to install **communitasit/eloquent-flowable**.

    composer require communitasit/eloquent-flowable

Next, run the following command to publish the configuration, migration, and seeder files.

    php artisan flowable:install

Don't forget to register the provider in **config/app.php**

    'providers' => [
        Illuminate\..\..
        ..,

        CommunitasIT\Flowable\FlowableServiceProvider::class,
    ]

The configuration, migration, and seeder files may be published separately.

    php artisan vendor:publish --provider "CommunitasIT\Flowable\FlowableServiceProvider" --tag="config"

    php artisan vendor:publish --provider "CommunitasIT\Flowable\FlowableServiceProvider" --tag="migrations"

    php artisan vendor:publish --provider "CommunitasIT\Flowable\FlowableServiceProvider" --tag="seeders"

## Structure

The topmost models in the workflows hierarchy are the ***CommunitasIT\Flowable\Models\Workflow*** and ***CommunitasIT\Flowable\Models\WorkflowStatus*** models.

To create a **Workflow**, call the **->new()** function from the **workflows()** helper function. The name of each **Workflow** should be unique.

    workflows()->new("Three-step Workflow");

To create a new **Workflow Status**, call the **->newStatus()** function from the same helper function. a unique name, description, and valid type should be supplied as parameters. Valid status types may be found and configured in the **flowable.php** configuration file.

    workflows()->newStatus(
        "New Status",
        "Status Description",
        "new"
    );

The next model in the workflows hierarchy is the ***CommunitasIT\Flowable\Models\WorkflowPlace*** model. This determines the current place of a model that implements workflows. A **Workflow Place** must be owned by a **Workflow** and tagged to a **Workflow Place**

To add a **Workflow Place** to a workflow, use a **Workflow**'s **->addPlace()** function. A **Workflow Status** instance must be passed as a parameter. Each **Workflow Place** name and **Workflow Status** tag must be unique per workflow.

    $workflow->addPlace(
        "New",
        $status
    );

The last model in the workflows hierarchy is the ***CommunitasIT\Flowable\Models\WorkflowTransitionModel***. These models act as links between **Workflow Places** and dictate the flow of the workflow.

To add a transition to a workflow, use any of the following functions. Both **Workflow Places** must be owned by the same workflow and a transition name must be supplied.

    $workflow->addTransition(
        $new_place, 
        $inprogress_place, 
        "Start Progress"
    );

    $inprogress_place->addTransitionTo(
        $done_place, 
        "Close Ticket"
    );

    $new_place->addTransitionFrom(
        $done_place, 
        "Re-open ticket"
    );

Finally, a **Workflow Place** owned by a **Workflow** must be assigned as a starting step for whenever a flowable model is created. Use the **->setStartingPlace()** function to assign a starting place. A **Workflow Place** object must be supplied.

    $workflow->setStartingPlace($place);

## Usage

To enable workflows on a model, include the ***CommunitasIT\Flowable\Flowable*** trait.

    <?php

    namespace App\Models;

    use Illuminate\Database\Eloquent\Model;
    use CommunitasIT\Flowable\Flowable;

    class Ticket extends Model
    {
        use Flowable;

        protected $table = 'tickets';
    }

To assign a model to a workflow, use the static function **assignWorkflow()**

    \App\Models\Ticket::assignWorkflow($workflow);

In the example above, after assignment of workflow to a model, each instance of ***App\Models\Ticket*** will be initialized with the starting step of its workflow. Every retrieved instance will be initialized too, if they haven't yet.

To transition a flowable model, call the **->transition()** function through the model and supply a valid **Workflow Transition** object.

    $ticket->transition($transition);

To get available transitions from a ticket's current place, use the **->getAvailableTransitions()** function.

    $transitions = $ticket->getAvailableTransitions();

## Configuration

The following may be configured in the published configuration file **flowable.php**

Status types used for **Workflow Status** may be configured by modifying the *"type"* key.

    'types' => [
        'new',
        'inprogress',
        'done',
        'void'
    ]