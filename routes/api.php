<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use \CommunitasIT\Flowable\Models\WorkflowTransition;

/**
 * /api prefix is not applied here because it is a package
 */
Route::prefix('api')->group(function(){
    Route::get('/flowable/get', function(Request $request){
        $model_type = $request->type;
        $model_id = $request->id;

        $model = $model_type::find($model_id);
        $model_place = $model->getPlace();
        $model_place_status = $model_place->status;

        return json_encode([
            "status" => $model_place->name,
            //"color" => sprintf('#%06X', mt_rand(0, 0xFFFFFF))
            "color" => $model_place_status->color
        ]);
    });

    Route::get('/flowable/transitions', function(Request $request){
        $model_type = $request->type;
        $model_id = $request->id;

        $model = $model_type::find($model_id);
        $model_place = $model->getPlace();

        return $model_place->transitions_to()->with('toPlace.status')->get()->toArray();
    });

    Route::post('/flowable/transition', function(Request $request){
        $model_type = $request->type;
        $model_id = $request->id;

        $model = $model_type::find($model_id);
        $transition = WorkflowTransition::find($request->transition_id);

        $model->transition($transition);
    });
});


