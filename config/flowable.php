<?php

return [
    /** Status types */
    'types' => [
        'new',
        'inprogress',
        'done',
        'void'
    ],

    /** Default status color */
    'default_color' => env('FLOWABLE_DEFAULT_COLOR', '"#d3d3d3'),

    /** Status colors */
    'color_new' => env('FLOWABLE_COLOR_NEW', "#d3d3d3"),
    'color_inprogress' => env('FLOWABLE_COLOR_INPROGRESS', "#0052CC"),
    'color_done' => env('FLOWABLE_COLOR_DONE', "#006644"),
    'color_void' => env('FLOWABLE_COLOR_VOID', "#dc3545"),
];