<?php

namespace CommunitasIT\Flowable\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
//use Illuminate\Broadcasting\PresenceChannel;
//use Illuminate\Broadcasting\PrivateChannel;
//use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ModelTransitioned implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $model, $old_place, $new_place, $transition, $new_color;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($model, $old, $new, $transition)
    {
        $this->model = $model;
        $this->old_place = $old;
        $this->new_place = $new;
        $this->transition = $transition;
        $this->new_color = $new->status->color;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('flowable.' . base64_encode(get_class($this->model)) . '.' . $this->model->getKey() ?? $this->model->id);
    }

    public function broadcastAs(){
        return 'flowable.transitioned';
    }
}
