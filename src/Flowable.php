<?php

namespace CommunitasIT\Flowable;

use CommunitasIT\Flowable\Models\WorkflowHasModel;
use CommunitasIT\Flowable\Models\WorkflowTransition;
use CommunitasIT\Flowable\Models\ModelState;

use Illuminate\Support\Str;

use CommunitasIT\Flowable\FlowableObserver;

use CommunitasIT\Flowable\Exceptions\ModelBelongsToAWorkflowException;

trait Flowable
{
    use \CommunitasIT\Flowable\Traits\CanBeTransitioned;
    use \CommunitasIT\Flowable\Traits\WorkflowAuditable;
    use \CommunitasIT\Flowable\Traits\CanInitializeWorkflow;

    public static function bootFlowable(){
        static::observe(new FlowableObserver());
    }

    public static function getWorkflow(){
        try{
            return WorkflowHasModel::where('model_type', get_class(new self))->firstOrFail()->workflow;
        }catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e){
            return null;
        }
    }

    public static function assignWorkflow($workflow){
        try{
            return WorkflowHasModel::create([
                'model_type' => get_class(new self),
                'workflow_id' => $workflow->getKey() ?? $workflow->id
            ]);
        }catch(\Illuminate\Database\QueryException $e){
            if(Str::contains($e->getMessage(), 'workflow_has_models.model_type')){
                throw new ModelBelongsToAWorkflowException("Model \"" . get_class(new self) . "\" already belongs to a workflow.");
            }

            throw $e;
        }
    }

    public function getPlace(){
        return $this->model_state->place ?? null;
    }

    public function model_state(){
        return $this->morphOne(ModelState::class, 'flowable');
    }

    public function getAvailableTransitions(){
        $place = $this->getPlace();

        return $place->transitions_to;
    }
}