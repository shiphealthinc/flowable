<?php

namespace CommunitasIT\Flowable;

class FlowableObserver
{
    public function created($model){
        $model->initializeWorkflow();
    }

    public function retrieved($model){
        if(!$model->model_state){
            $model->initializeWorkflow();
        }
    }
}