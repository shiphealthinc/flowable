<?php

namespace CommunitasIT\Flowable\Exceptions;

use Exception;

class ModelDoesNotBelongToAnyWorkflowException extends Exception
{
    public function render(){

    }
}