<?php

namespace CommunitasIT\Flowable\Exceptions;

use Exception;

class PlaceCannotLeadToSelfException extends Exception
{
    public function render(){

    }
}