<?php

namespace CommunitasIT\Flowable\Models;

use Illuminate\Database\Eloquent\Model;

class WorkflowAudit extends Model
{
    use \CommunitasIT\Flowable\Traits\WorkflowAudit;

    protected $guarded = [];
    protected $table = 'workflow_audits';
    public $timestamps = false;

    public function flowable(){
        return $this->morphTo();
    }
}