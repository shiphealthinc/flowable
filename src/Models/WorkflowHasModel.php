<?php

namespace CommunitasIT\Flowable\Models;

use Illuminate\Database\Eloquent\Model;

use CommunitasIT\Flowable\Models\Workflow;

class WorkflowHasModel extends Model
{
    protected $guarded = [];
    protected $table = 'workflow_has_models';

    public function workflow(){
        return $this->hasOne(Workflow::class, 'id', 'workflow_id');
    }

    public function getModel(){
        return resolve($this->model_type);
    }
}