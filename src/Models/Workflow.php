<?php

namespace CommunitasIT\Flowable\Models;

use Illuminate\Database\Eloquent\Model;

class Workflow extends Model
{
    use \CommunitasIT\Flowable\Traits\Workflow;

    protected $guarded = [];
    protected $table = 'workflows';

    protected $appends = [
        'models',
        'clean_models'
    ];

    public function transitions(){
        return $this->hasManyThrough(
            WorkflowTransition::class,
            WorkflowPlace::class,
            'workflow_id',
            'from_place_id',
            'id',
            'id'
        );
    }

    public function places(){
        return $this->hasMany(WorkflowPlace::class, 'workflow_id');
    }

    public function starting_place(){
        return $this->hasOne(WorkflowPlace::class, 'id', 'starting_place_id');
    }

    public function getModelsAttribute(){
        return \DB::table('workflow_has_models')
            ->where('workflow_id', $this->id)
            ->get()
            ->pluck('model_type')
            ->toArray();
    }

    public function getCleanModelsAttribute(){
        return collect($this->models)->map(function($model_class){
            return class_basename($model_class);
        })->toArray();
    }
}