<?php

namespace CommunitasIT\Flowable\Models;

use Illuminate\Database\Eloquent\Model;

class WorkflowTransition extends Model
{
    use \CommunitasIT\Flowable\Traits\WorkflowTransition;

    protected $guarded = [];
    protected $table = 'workflow_has_transitions';

    public function getWorkflow(){
        return $this->fromPlace->workflow;
    }

    public function fromPlace(){
        return $this->hasOne(WorkflowPlace::class, 'id', 'from_place_id');
    }

    public function toPlace(){
        return $this->hasOne(WorkflowPlace::class, 'id', 'to_place_id');
    }
}