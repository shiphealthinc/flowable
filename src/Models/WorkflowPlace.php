<?php

namespace CommunitasIT\Flowable\Models;

use Illuminate\Database\Eloquent\Model;

class WorkflowPlace extends Model
{
    use \CommunitasIT\Flowable\Traits\WorkflowPlace;

    protected $guarded = [];
    protected $table = 'workflow_has_places';

    protected $appends = [
        'is_starting_step',
        'model_count'
    ];

    public function workflow(){
        return $this->belongsTo(Workflow::class, 'workflow_id');
    }

    public function transitions_to(){
        return $this->hasMany(WorkflowTransition::class, 'from_place_id');
    }

    public function transitions_from(){
        return $this->hasMany(WorkflowTransition::class, 'to_place_id');
    }

    public function status(){
        return $this->hasOne(WorkflowStatus::class, 'id', 'status_id');
    }

    public function model_states(){
        return $this->hasMany(ModelState::class, 'place_id');
    }

    public function getIsStartingStepAttribute(){
        return $this->isStartingStep();
    }

    public function getModelCountAttribute(){
        return $this->model_states()->count();
    }
}