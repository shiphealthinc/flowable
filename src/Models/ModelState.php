<?php

namespace CommunitasIT\Flowable\Models;

use Illuminate\Database\Eloquent\Model;

class ModelState extends Model
{
    use \CommunitasIT\Flowable\Traits\ModelState;

    protected $guarded = [];
    protected $table = 'model_states';
    public $timestamps = false;
    protected $casts = [
        'place_id' => 'integer'
    ];

    public function flowable(){
        return $this->morphTo();
    }

    public function place(){
        return $this->hasOne(WorkflowPlace::class, 'id', 'place_id');
    }
}