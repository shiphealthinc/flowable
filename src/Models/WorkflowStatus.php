<?php

namespace CommunitasIT\Flowable\Models;

use Illuminate\Database\Eloquent\Model;

class WorkflowStatus extends Model
{
    use \CommunitasIT\Flowable\Traits\WorkflowStatus;

    protected $guarded = [];
    protected $table = 'workflow_statuses';
    protected $appends = [
        'color'
    ];

    public function places(){
        return $this->hasMany(WorkflowPlace::class, 'status_id');
    }
}