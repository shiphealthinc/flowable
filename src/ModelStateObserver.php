<?php

namespace CommunitasIT\Flowable;

use CommunitasIT\Flowable\Models\WorkflowPlace;
use CommunitasIT\Flowable\Models\WorkflowAudit;

use CommunitasIT\Flowable\Events\ModelTransitioned;

class ModelStateObserver
{
    public function creating($model_state){
        $flowable_type = $model_state->flowable_type;

        if($workflow = $flowable_type::getWorkflow()){
            if($place = $workflow->starting_place){
                $model_state->place_id = $place->getKey() ?? $place->id();
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    public function updated($model_state){
        $old_place = WorkflowPlace::find($model_state->getOriginal()['place_id']);
        $new_place = WorkflowPlace::find($model_state->place_id);
        $transition = $old_place->getTransitionTo($new_place);

        $audit = $model_state->flowable->workflow_audits()->create([
            'old_place_name' => $old_place->name,
            'old_place_color' => config(
                'flowable.color_' . $old_place->status->type,
                config('flowable.default_color')
            ),
            'transition_name' => $transition->name,
            'new_place_name' => $new_place->name,
            'new_place_color' => config(
                'flowable.color_' . $new_place->status->type,
                config('flowable.default_color')
            ),
            'user_id' => auth()->user()->id ?? null
        ]);

        broadcast(new ModelTransitioned(
            $model_state->flowable,
            $old_place,
            $new_place,
            $transition
        ));
    }
}