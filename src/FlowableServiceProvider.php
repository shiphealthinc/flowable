<?php

namespace CommunitasIT\Flowable;

//use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;

use CommunitasIT\Flowable\Console\InstallCommand;

class FlowableServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the service provider.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPublishing();
        $this->mergeConfigFrom(__DIR__.'/../config/flowable.php', 'flowable');
        $this->loadRoutesFrom(__DIR__.'/../routes/api.php');

        $this->app->singleton(WorkflowHelper::class, function(){
            return new WorkflowHelper();
        });
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->commands([
            InstallCommand::class,
        ]);
    }

    /**
     * Register the package's publishable resources.
     *
     * @return void
     */
    private function registerPublishing()
    {
        if ($this->app->runningInConsole()) {
            // Lumen lacks a config_path() helper, so we use base_path()
            $this->publishes([
                __DIR__.'/../config/flowable.php' => base_path('config/flowable.php'),
            ], 'config');

            /** Publish stubbed migrations */
            $this->publishes($this->getMigrationsToPublish(), 'migrations');

            /** Publish stubbed seeders */
            $this->publishes($this->getSeedersToPublish(), 'seeders');
        }
    }

    private function getMigrationsToPublish(){
        $migrations_to_publish = [];

        foreach(glob(__DIR__ . '/../database/migrations/*.stub') as $stub){
            $table_name = basename($stub, '.stub');
            $migrations_to_publish[$stub] = database_path(sprintf('migrations/%s_create_' . $table_name . '_table.php', date('Y_m_d_His')));
        }

        return $migrations_to_publish;
    }

    private function getSeedersToPublish(){
        $seeders_to_publish = [];

        foreach(glob(__DIR__ . '/../database/seeders/*.stub') as $stub){
            $seeder_name = basename($stub, '.stub');
            $seeders_to_publish[$stub] = database_path('seeders/' . $seeder_name . '.php');
        }

        return $seeders_to_publish;
    }
}