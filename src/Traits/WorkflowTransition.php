<?php

namespace CommunitasIT\Flowable\Traits;

use CommunitasIT\Flowable\Models\WorkflowPlace as Place;

use CommunitasIT\Flowable\Exceptions\PlacesBelongToDifferentWorkflowsException;

trait WorkflowTransition
{
    public static function bootWorkflowTransition(){
        static::creating(function($transition){
            $fromPlace = Place::find($transition->from_place_id);
            $toPlace = Place::find($transition->to_place_id);
            if($fromPlace->workflow->id != $toPlace->workflow->id){
                throw new PlacesBelongToDifferentWorkflowsException("Supplied workflow places do not belong to the same workflow.");
            }
        });
    }
}