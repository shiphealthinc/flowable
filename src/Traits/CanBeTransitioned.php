<?php

namespace CommunitasIT\Flowable\Traits;

use CommunitasIT\Flowable\Exceptions\InvalidTransitionException;

trait CanBeTransitioned
{
    /**
     * Transitions the model.
     * 
     * @param object @transition_or_place
     * 
     * @throws InvalidWorkflowTransitionException if parameter is not WorkflowPlace or WorkflowTransition
     */
    public function transition($transition_or_place){
        /** enter code to transition model here and write audits */
        if(class_basename($transition_or_place) == "WorkflowPlace"){
            $place = $transition_or_place;
        }else if(class_basename($transition_or_place) == "WorkflowTransition"){
            $transition = $transition_or_place;

            if($this->getAvailableTransitions()->contains($transition)){
                return $this->model_state->update([
                    'place_id' => $transition->toPlace->id
                ]);
            }else{
                throw new InvalidTransitionException("The transition \"" . $transition->name . "\" cannot be used from place \"" . $this->getPlace()->name . "\".");
            }
        }else{
            throw new InvalidTransitionException("Invalid parameter supplied when attempting to transition model. Supplied an instance of \"" . get_class($transition_or_place) . "\". Expected a Place or Transition instance.");
        }
    }
}