<?php

namespace CommunitasIT\Flowable\Traits;

use CommunitasIT\Flowable\ModelStateObserver;

use CommunitasIT\Flowable\Exceptions\ModelDoesNotBelongToAnyWorkflowException;

trait ModelState
{
    public static function bootModelState(){
        static::observe(new ModelStateObserver());
    }
}