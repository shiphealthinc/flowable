<?php

namespace CommunitasIT\Flowable\Traits;

trait WorkflowAudit
{
    public static function bootWorkflowAudit(){
        static::creating(function($audit){
            if(!$audit->transitioned_at){
                $audit->transitioned_at = now();
            }
        });
    }
}