<?php

namespace CommunitasIT\Flowable\Traits;

trait WorkflowAuditable
{
    public static function bootWorkflowAuditable(){

    }

    public function workflow_audits(){
        return $this->morphMany(\CommunitasIT\Flowable\Models\WorkflowAudit::class, 'flowable');
    }
}