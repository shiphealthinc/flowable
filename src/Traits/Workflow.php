<?php

namespace CommunitasIT\Flowable\Traits;

use CommunitasIT\Flowable\Exceptions\StatusExistsForWorkflow;
use CommunitasIT\Flowable\Exceptions\TransitionExists;
use CommunitasIT\Flowable\Exceptions\PlaceIsNotOwnedByWorkflowException;
use CommunitasIT\Flowable\Exceptions\PlaceNameExistsForWorkflow;
use CommunitasIT\Flowable\Exceptions\PlaceCannotLeadToSelfException;
use CommunitasIT\Flowable\Exceptions\WorkflowExistsException;

use CommunitasIT\Flowable\Models\Workflow as WorkflowModel;

use Illuminate\Support\Str;

trait Workflow
{
    public static function bootWorkflow(){
        static::creating(function($workflow){
            if(WorkflowModel::where('name', $workflow->name)->exists()){
                throw new WorkflowExistsException("A workflow with the name \"" . $workflow->name . "\" already exists.");
            }
        });
    }

    public function setStartingPlace($place){
        if($this->places->contains($place)){
            $this->starting_place_id = $place->getKey() ?? $place->id;
            $this->save();
        }else{
            throw new PlaceIsNotOwnedByWorkflowException("Supplied place is not owned by the selected workflow.");
        }
    }

    public function assignToModel($model){
        $model_class = get_class($model);

        $model_class::assignWorkflow($this);
    }

    public function addPlace($name, $status){
        try{
            return $this->places()->create([
                'name' => $name,
                'status_id' => $status->getKey() ?? $status->id
            ]);
        }catch(\Illuminate\Database\QueryException $e){
            if(Str::contains($e->getMessage(), 'workflow_has_places_workflow_id_status_id_unique')){
                throw new StatusExistsForWorkflow("The status \"" . $status->name . "\" has already been used for workflow \"" . $this->name . "\".");
            }else if(Str::contains($e->getMessage(), 'workflow_has_places_workflow_id_name_unique')){
                throw new PlaceNameExistsForWorkflow("The workflow place name \"" . $name . "\" has already been used for workflow \"" . $this->name . "\".");
            }

            throw $e;
        }
    }

    public function addTransition($name, $from_place, $to_place){
        if((($this->getKey() ?? $this->id) != ($from_place->workflow->getKey() ?? $from_place->workflow->id)) || (($this->getKey() ?? $this->id) != ($to_place->workflow->getKey() ?? $to_place->workflow->id))){
            throw new PlaceIsNotOwnedByWorkflowException("One or both of supplied workflow places do not belong to the selected workflow.");
        }

        if($from_place->id == $to_place->id){
            throw new PlaceCannotLeadToSelfException("A workflow step may not lead to itself via a transition.");
        }

        try{
            return $this->transitions()->create([
                'name' => $name,
                'from_place_id' => $from_place->getKey() ?? $from_place->id,
                'to_place_id' => $to_place->getKey() ?? $to_place->id
            ]);
        }catch(\Illuminate\Database\QueryException $e){
            if(Str::contains($e->getMessage(), 'workflow_has_transitions_from_place_id_to_place_id_unique')){
                throw new TransitionExists("A transition from \"" . $from_place->name . "\" to \"" . $to_place->name . "\" already exists.");
            }

            throw $e;
        }
    }
}