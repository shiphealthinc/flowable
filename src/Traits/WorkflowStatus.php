<?php

namespace CommunitasIT\Flowable\Traits;

use CommunitasIT\Flowable\Exceptions\StatusTypeNotFoundException;
use CommunitasIT\Flowable\Exceptions\StatusCannotBeDeletedException;

trait WorkflowStatus
{
    public static function bootWorkflowStatus(){
        static::creating(function($status){
            if(!in_array($status->type, config('flowable.types'))){
                throw new StatusTypeNotFoundException("Workflow status type \"" . $status->type . "\" not found in configuration. Available types are (" . implode(", ", config('flowable.types')) . ")");
            }
        });

        static::updating(function($status){
            if(!in_array($status->type, config('flowable.types'))){
                throw new StatusTypeNotFoundException("Workflow status type \"" . $status->type . "\" not found in configuration. Available types are (" . implode(", ", config('flowable.types')) . ")");
            }
        });

        static::deleting(function($status){
            if($status->places()->exists()){
                throw new StatusCannotBeDeletedException("Workflow status cannot be deleted because it has attached workflow steps.");
            }
        });
    }

    public function getColorAttribute(){
        return config(
            'flowable.color_' . $this->type,
            config('flowable.default_color')
        );
    }
}