<?php

namespace CommunitasIT\Flowable\Traits;

trait CanInitializeWorkflow
{
    public function initializeWorkflow(){
        return $this->model_state()->create();
    }
}