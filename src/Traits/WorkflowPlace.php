<?php

namespace CommunitasIT\Flowable\Traits;

use CommunitasIT\Flowable\Models\WorkflowPlace as Place;
use CommunitasIT\Flowable\Models\WorkflowTransition as Transition;

trait WorkflowPlace
{
    public static function bootWorkflowPlace(){

    }

    public function addTransitionTo($place, $name){
        return $this->workflow->addTransition($name, $this, $place);
    }

    public function addTransitionFrom($place, $name){
        return $place->addTransitionTo($this, $name);
    }

    public function getTransitionTo($place){
        try{
            return $this->transitions_to()->where('to_place_id', $place->id)->firstOrFail();
        }catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e){

            return null;
        }
    }

    public function getTransitionFrom($place){
        return $place->getTransitionTo($this);
    }

    public function isStartingStep(){
        return $this->id == $this->workflow->starting_place_id;
    }
}