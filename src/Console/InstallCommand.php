<?php

namespace CommunitasIT\Flowable\Console;

use Illuminate\Console\Command;
use Illuminate\Container\Container;
use Illuminate\Support\Str;

class InstallCommand extends Command
{
    protected $signature = 'flowable:install';
    protected $description = 'Install all of the Flowable resources';

    public function handle()
    {
        $this->comment('Publishing Flowable Configuration...');
        $this->callSilent('vendor:publish', [
            '--tag' => 'config',
            '--provider' => 'CommunitasIT\Flowable\FlowableServiceProvider',
        ]);

        $this->comment('Publishing Flowable Migrations...');
        $this->callSilent('vendor:publish', [
            '--tag' => 'migrations',
            '--provider' => 'CommunitasIT\Flowable\FlowableServiceProvider',
        ]);

        $this->comment('Publishing Flowable Seeders...');
        $this->callSilent('vendor:publish', [
            '--tag' => 'seeders',
            '--provider' => 'CommunitasIT\Flowable\FlowableServiceProvider',
        ]);

        // $this->comment('Publishing Flowable Files...');
        // $this->callSilent('vendor:publish --provider "CommunitasIT\Flowable\FlowableServiceProvider"');

        /** This does not work */
        //$this->registerFlowableServiceProvider();

        $this->info('CommunitasIT\\Flowable installed successfully!');
    }

    /**
     * Register the Flowable service provider in the application configuration file.
     *
     * @return void
     */
    protected function registerFlowableServiceProvider()
    {
        $namespace = Str::replaceLast('\\', '', Container::getInstance()->getNamespace());

        $appConfig = file_get_contents(config_path('app.php'));

        if (Str::contains($appConfig, 'CommunitasIT\\Flowable\\FlowableServiceProvider::class')) {
            return;
        }

        file_put_contents(config_path('app.php'), str_replace(
            "{$namespace}\\Providers\EventServiceProvider::class,".PHP_EOL,
            "{$namespace}\\Providers\EventServiceProvider::class,".PHP_EOL."        CommunitasIT\Flowable\FlowableServiceProvider::class,".PHP_EOL,
            $appConfig
        ));
    }
}