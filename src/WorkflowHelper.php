<?php

namespace CommunitasIT\Flowable;

use CommunitasIT\Flowable\Models\Workflow;
use CommunitasIT\Flowable\Models\WorkflowStatus as Status;
use CommunitasIT\Flowable\Models\WorkflowPlace as Place;
use CommunitasIT\Flowable\Models\WorkflowTransition as Transition;

use CommunitasIT\Flowable\Exceptions\StatusNameExistsException;

use Illuminate\Support\Str;

class WorkflowHelper
{
    public function new($name){
        return Workflow::create([
            'name' => $name
        ]);
    }

    public function newStatus($name, $description, $type){
        try{
            return Status::create([
                'name' => $name,
                'description' => $description,
                'type' => $type
            ]);
        }catch(\Illuminate\Database\QueryException $e){
            if(Str::contains($e->getMessage(), 'workflow_statuses_name_unique')){
                throw new StatusNameExistsException("A workflow status with the name \"" . $name . "\" already exists.");
            }

            throw $e;
        }  
        
    }

    public function first(){
        return Workflow::first();
    }

    public function query(){
        return Workflow::query();
    }

    public function get($closure){
        $query = $this->query();

        return $closure($query)->get();
    }

    public function statuses(){
        return Status::query();
    }

    public function types(){
        return config('flowable.types', []);
    }

    public function places(){
        return Place::query();
    }

    public function transitions(){
        return Transition::query();
    }
}